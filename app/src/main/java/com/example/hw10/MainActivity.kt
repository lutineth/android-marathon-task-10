package com.example.hw10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val edLastName: EditText = findViewById(R.id.editTextLastName)
        val edFirstName: EditText = findViewById(R.id.editTextFirstName)
        val edMiddleName: EditText = findViewById(R.id.editTextMiddleName)
        val edAge: EditText = findViewById(R.id.editTextAge)
        val edHobby: EditText = findViewById(R.id.editTextHobby)

        val button: Button = findViewById(R.id.buttonInfo)
        button.setOnClickListener{
            PersonalInfo.lastName = edLastName.text.toString()
            PersonalInfo.firstName= edFirstName.text.toString()
            PersonalInfo.middleName = edMiddleName.text.toString()
            PersonalInfo.age = edAge.text.toString()
            PersonalInfo.hobby = edHobby.text.toString()
            Intent(this, InfoActivity::class.java).also {
                startActivity(it)
            }
        }
    }
}
