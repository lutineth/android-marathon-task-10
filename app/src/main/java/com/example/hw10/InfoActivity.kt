package com.example.hw10

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val lastname: String = PersonalInfo.lastName
        val firstname: String = PersonalInfo.firstName
        val middlename: String = PersonalInfo.middleName
        val age: String = PersonalInfo.age
        val hobby: String = PersonalInfo.hobby

        val textInfo: TextView = findViewById(R.id.textInfo)
        textInfo.text = when(age.toInt()){
            in 0..12 -> "Привет! Вот информация о тебе:\nФамилия:$lastname\nИмя:$firstname\nОтчество:$middlename\nВозраст:$age\nХобби:$hobby\n"
            in 13..19 -> "Здравствуй, друг! Твои введенные данные:\nФамилия:$lastname\nИмя:$firstname\nОтчество:$middlename\nВозраст:$age\nХобби:$hobby\n"
            in 20..39 -> "Здравствуйте, уважаемый(ая)! Вами были введены следующие данные:\nФамилия:$lastname\nИмя:$firstname\nОтчество:$middlename\nВозраст:$age\nХобби:$hobby\n"
            in 40..99 -> "Здравствуйте! Вы ввели следующие данные о своей персоне:\nФамилия:$lastname\nИмя:$firstname\nОтчество:$middlename\nВозраст:$age\nХобби:$hobby\n"
            else -> "Здравствуйте, вы все еще живы? Ваши введенные данные:\nФамилия:$lastname\nИмя:$firstname\nОтчество:$middlename\nВозраст:$age\nХобби:$hobby\n"
        }
    }
}
